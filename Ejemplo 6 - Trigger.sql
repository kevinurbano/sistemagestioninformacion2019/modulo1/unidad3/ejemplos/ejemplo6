﻿USE ejemplo6u3;

/* 
  Ej1 .- Crear un disparador para la tabla ventas para que cuando 
          metas un registro nuevo te calcule el total automáticamente. 
          El total debe ser unidades por precio. 
          Recordar que para el insert solo podéis utilizar NEW. Además, 
          debéis recordar que para actualizar los valores de una tabla en la inserción debe ser BEFORE (con el AFTER podéis actualizar otra tabla, pero no la misma). 
*/

DELIMITER //
CREATE OR REPLACE TRIGGER ej1BI
  BEFORE INSERT  
    ON ventas
    FOR EACH ROW
  BEGIN
    SET NEW.total = NEW.unidades * NEW.precio; 
  END //
DELIMITER ;

INSERT INTO ventas (producto, precio, unidades)
  VALUES ('p6', 12, 3);
INSERT INTO ventas (producto, precio, unidades)
  VALUES ('p6', 15, 4);

SELECT * FROM ventas v;

/*
  Ej2 - Crear un disparador para la tabla ventas para que cuando inserte un registro
        me sume el total a la tabla productos (en el campo cantidad).   
*/
DELIMITER //
CREATE OR REPLACE TRIGGER ej2AI
AFTER INSERT
  ON ventas
  FOR EACH ROW
BEGIN 

  UPDATE productos p
    SET p.cantidad= p.cantidad + NEW.total WHERE p.producto=NEW.producto;

END //
DELIMITER ;

INSERT INTO ventas (producto, precio, unidades)
  VALUES ('p1', 24, 2);

SELECT * FROM productos p;

/* 
  Ej3 - Crear un disparador para la tabla ventas para que cuando
        actualices un registro nuevo te calcule el total automáticamente.

        El total debe ser unidades por precio.
*/
DELIMITER //

CREATE OR REPLACE TRIGGER ej3BU
BEFORE UPDATE 
  ON ventas
  FOR EACH ROW
BEGIN
  SET NEW.total = NEW.unidades * NEW.precio;
END //

DELIMITER ;

UPDATE ventas v
  SET v.precio=14 WHERE id=15; -- producto nuevo p6 para probar trigger

SELECT * FROM ventas v;

/* 
  Ej4 - Crear un disparador para la tabla ventas para que cuando actualice
        un registro me sume el total a la tabla productos (en el campo cantidad).   
*/
DELIMITER //
CREATE OR REPLACE TRIGGER Ej4AU
  AFTER UPDATE   
    ON ventas
    FOR EACH ROW
  BEGIN
     
     UPDATE productos p
      SET 
          p.cantidad = p.cantidad+(NEW.total-OLD.total)
      WHERE p.producto=NEW.producto;        
  END //
DELIMITER ;

SELECT * FROM ventas v WHERE v.id=2;


UPDATE ventas v SET v.precio=10 WHERE v.id=2;


SELECT * FROM productos p;

/*
  Ej5 - Crear un disparador para la tabla productos que si cambia 
        el código del producto te sume todos los totales de ese producto de la tabla ventas.
*/
DELIMITER //
CREATE OR REPLACE TRIGGER ej5BU
  BEFORE UPDATE   
    ON productos
    FOR EACH ROW
  BEGIN
     SET NEW.cantidad = (SELECT SUM(v.total) FROM ventas v WHERE v.producto=new.producto);
  END //
DELIMITER ;

SELECT * FROM productos p;

UPDATE productos p
  SET p.producto='p6' WHERE p.producto='p1';

SELECT SUM(total) FROM ventas WHERE producto='p6';

SELECT * FROM productos p;

UPDATE productos p
  SET p.producto='p1' WHERE p.producto='p6';

SELECT SUM(total) FROM ventas WHERE producto='p1';

SELECT * FROM productos p;

/*
  Ej6 - Crear un disparador para la tabla productos que si eliminas
        un producto te elimine todos los productos del mismo código
        en la tabla ventas 
*/
/* Nos da conflicto al querer eliminar un producto teniendo activado el trigger del ejercicio 7 
   para resolver eso reemplazamos el trigger con un foreing key de tipo on delete cascade */
DROP TRIGGER ej6AD;

DELIMITER //
CREATE OR REPLACE TRIGGER ej6AD
  AFTER DELETE   
    ON productos
    FOR EACH ROW
  BEGIN
     DELETE 
      FROM 
        ventas 
      WHERE 
        producto=OLD.producto;
  END //
DELIMITER ;

SELECT * FROM productos p WHERE p.producto='p5'; 
DELETE FROM productos WHERE producto='p5'; -- no puedo hacer delete
 
SELECT * FROM ventas v WHERE id=14; 
DELETE FROM ventas WHERE id=14; -- me permite hacer delete  

/*
  Ej7 - Crear un disparador para la tabla ventas que si eliminas
        un registro te reste el total del campo cantidad
        de la tabla productos (en el campo cantidad).  
*/
DELIMITER //
CREATE OR REPLACE TRIGGER ej7AD
  AFTER DELETE  
    ON ventas
    FOR EACH ROW
  BEGIN
     UPDATE productos p
      SET p.cantidad = p.cantidad - OLD.total WHERE p.producto=OLD.producto;
  END //
DELIMITER ;

SELECT * FROM ventas v WHERE v.id=1;

DELETE FROM ventas WHERE id=1;

SELECT * FROM productos p; 

/*
  Ej8 - Modificar el disparador 3 para que modifique la tabla productos 
        actualizando el valor del campo cantidad en funcion del total.
*/
-- Borrando trigger ej3BU
DROP TRIGGER ej3BU;

DELIMITER //
CREATE OR REPLACE TRIGGER ej8BU
  BEFORE UPDATE   
    ON ventas
    FOR EACH ROW
  BEGIN
     SET NEW.total = NEW.precio * NEW.unidades;

     UPDATE productos p
      SET p.cantidad=p.cantidad+(NEW.total-OLD.total) WHERE p.producto=new.producto;
  END //
DELIMITER ;


SELECT * FROM ventas v WHERE v.id=2;


UPDATE ventas v SET v.precio=10 WHERE v.id=2;


SELECT * FROM productos p;